***************************
API reference
***************************

.. autosummary::
   :toctree:

   pathwalker.path_is_relative_to
   pathwalker.keep_root_paths
   pathwalker.walk_folder_paths
   pathwalker.walk_file_paths
