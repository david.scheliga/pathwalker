# pathwalker 
[![Coverage Status](https://coveralls.io/repos/gitlab/david.scheliga/pathwalker/badge.svg?branch=release)](https://coveralls.io/gitlab/david.scheliga/pathwalker?branch=release)
[![Build Status](https://travis-ci.com/david.scheliga/pathwalker.svg?branch=release)](https://travis-ci.com/david.scheliga/pathwalker)
[![PyPi](https://img.shields.io/pypi/v/pathwalker.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/pathwalker/)
[![Python Versions](https://img.shields.io/pypi/pyversions/pathwalker.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/pathwalker/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Documentation Status](https://readthedocs.org/projects/pathwalker/badge/?version=latest)](https://pathwalker.readthedocs.io/en/latest/?badge=latest)

![pathwalker-icon](https://pathwalker.readthedocs.io/en/latest/_images/pathwalker-icon.svg)

## Installation

```` shell script
    $ pip install pathwalker
````

## Basic Usage

[Read-the-docs](https://pathwalker.readthedocs.io/en/latest/index.html) for a more detailed documentation.

## Contribution

Any contribution by reporting a bug or desired changes are welcomed. The preferred 
way is to create an issue on the gitlab's project page, to keep track of everything 
regarding this project.

### Contribution of Source Code
#### Code style
This project follows the recommendations of [PEP8](https://www.python.org/dev/peps/pep-0008/).
The project is using [black](https://github.com/psf/black) as the code formatter.

#### Workflow

1. Fork the project on Gitlab.
2. Commit changes to your own branch.
3. Submit a **pull request** from your fork's branch to our branch *'dev'*.

## Authors

* **David Scheliga** 
    [@gitlab](https://gitlab.com/david.scheliga)
    [@Linkedin](https://www.linkedin.com/in/david-scheliga-576984171/)
    - Initial work
    - Maintainer

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the
[LICENSE](LICENSE) file for details

## Acknowledge

[Code style: black](https://github.com/psf/black)
